from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from db.base import insert_table


replace_keyborde = types.ReplyKeyboardRemove()


class UserForm(StatesGroup):
    name = State()
    age = State()
    gender = State()
    country = State()
    interes = State()


async def start_survey(message: types.Message):
    await UserForm.name.set()
    await message.answer("как вас зовут?", reply_markup=replace_keyborde)


async def process_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['name'] = message.text
    await UserForm.next()
    await message.answer('сколько вам лет?')


async def process_age(message: types.Message, state: FSMContext):
    if not message.text.isdigit():
        await message.answer('пишите только числа')
    elif int(message.text) < 9 or int (message.text) > 51:
        await message.answer('возрастное ограничене')
    else:
        async with state.proxy() as data:
            data['age'] = message.text
            await UserForm.next()
            kb = types.ReplyKeyboardMarkup(resize_keyboard=True).add(types.KeyboardButton('мужчина'), types.KeyboardButton('женщина'),
                                                 types.KeyboardButton('неизвестно'))
            await message.answer('ваш пол:', reply_markup=kb)


async def process_gender(message: types.Message, state: FSMContext):
    gender = ['мужчина', 'женщина', 'неизвестно']
    if not message.text in gender:
        await message.answer('выберите из кнопок ниже')
    else:
        async with state.proxy() as data:
            data['gender'] = message.text
        await UserForm.next()
        await message.answer('введите вашу страну', reply_markup=replace_keyborde)


async def process_country(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['country'] = message.text
    await UserForm.next()
    male = types.KeyboardButton('мужчины')
    female = types.KeyboardButton('девушки')
    none = types.KeyboardButton('неважно')
    kb = types.ReplyKeyboardMarkup(resize_keyboard=True).add(male, female, none)
    await message.answer('кто вас интересует?', reply_markup=kb)


async def process_interes(message: types.Message, state: FSMContext):
    gender = ['мужчины', 'девушки', 'неважно']

    if not message.text in gender:
        await message.answer('выберите из предложенного ниже')
    else:
        async with state.proxy() as data:
            data['interes'] = message.text
            insert_table(data)
        await state.finish()
        await message.answer('спасибо что заполнили анкету', reply_markup=replace_keyborde)


def register_survey_handlers(dp: Dispatcher):
    dp.register_message_handler(start_survey, commands=["surv"])
    dp.register_message_handler(process_name, state=UserForm.name)
    dp.register_message_handler(process_age, state=UserForm.age)
    dp.register_message_handler(process_gender, state=UserForm.gender)
    dp.register_message_handler(process_country, state=UserForm.country)
    dp.register_message_handler(process_interes, state=UserForm.interes)


