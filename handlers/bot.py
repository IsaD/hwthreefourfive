from db.base import get_anketa
from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def anketa_info(person_id):
    kb = InlineKeyboardMarkup(resize_keyboard=True)
    kb.add(InlineKeyboardButton('показать анкеты', callback_data=f'show{person_id}'))


async def get_ank(message: types.Message):
    ank = get_anketa()
    for a in ank:
        await message.answer(f"ID: {a[0]}\n"
                             f"name: {a[1]}\n"
                             f"age: {a[2]}\n"
                             f"gender: {a[3]}\n"
                             f"country: {a[4]}\n"
                             f"Interes: {a[5]}\n")

        