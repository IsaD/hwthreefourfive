from aiogram import types

from config import bot, scheduler


async def handle_reminder(message: types.Message):
    await send_reminder(message.chat.id, message.text.replace('Напомни', ''))


async def reminder(message: types.Message):
    scheduler.add_job(
        handle_reminder,
        "interval",
        seconds=3,
        args=(message,)
    )
    await message.answer('напомню.')


async def send_reminder(chat_id, reminder_text):
    await bot.send_message(
        chat_id=chat_id,
        text=f"не забудь про: {reminder_text}")