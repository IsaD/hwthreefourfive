from aiogram import executor
from config import dp, scheduler
from handlers.FSM_Anketa import register_survey_handlers
from handlers.bot import get_ank
from handlers.reminder import reminder


if __name__ == "main":
    # dp.register_message_handler()
    scheduler.start()
    dp.register_message_handler(reminder)
    register_survey_handlers(dp)
    dp.register_message_handler(get_ank, commands=['show'])
    executor.start_polling(dispatcher=dp,
                           skip_updates=True,
                           )