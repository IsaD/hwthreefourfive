import sqlite3
from pathlib import Path
from config import bot

DB_NAME = 'db.sqlite3'  # 'products.db'
DB_PATH = Path(__file__).parent.parent
db = sqlite3.connect(DB_PATH / DB_NAME)
cur = db.cursor()
def init_db():
    global db, cursor

def create_table():
    cur.execute("""
    CREATE TABLE IF NOT EXISTS Anketa(
        person_id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        age INTEGER,
        gender TEXT NOT NULL,
        country TEXT,
        interes TEXT
        )
    """)
    db.commit()



def populate_tables():
    cur.executemany("""
        INSERT INTO Anketa(name, age, gender, country, interes)
        VALUES (?, ?, ?, ?, ?)
        """, [
        ('tif', 17, 'мужчина', 'Кыргызстан', 'девушки'),
        ('fit', 18, 'девушка', 'Америка', 'мужчины')
    ])
    db.commit()


def save_survey(data):
    data = data.as_dict()
    cur.execute(
        """
        INSERT INTO Anketa (name, age, gender, country, interes)
        VALUES (?, ?, ?, ?, ?)
        """,
        (data['name'], data['age'], data['gender'], data['country'], data['interes']))
    db.commit()


def get_anketa():
    cur.execute("""SELECT person_id, name, age, gender, country, interes FROM Anketa""")
    return cur.fetchall()


def insert_table(data):
    data = data.as_dict()
    cur.execute("""
        INSERT INTO FSM_AnketaZnakomstv(name, age, gender, country, interes):
        VALUES (:name, :age, :gender, :country, :interes)""",
                {'name': data['name'],
                 'age': data['age'],
                 'gender': data['gender'],
                 'country': data['country'],
                 'interes': data['interes']})
    db.commit()


if __name__ == 'main':
    create_table()
    populate_tables()